WUSTL Email Templates
=====================

A set of boilerplates for quickly creating HTML email templates that respect the current brand guidelines from the Office of Public Affairs.

## Basic Styles

* [Basic Example](email.html)
* [Formal Example](formal-email.html)

## Getting Started

This is a project that is meant to hack, so start by forking this repo into your own project and build your own templates from there.

This project uses Grunt to automate a bunch of stuff for you. In order to you use, you will need to [download and install node.js](http://nodejs.org/download/) and then, from the command line:

1. Install `grunt-cli` globally with `npm install -g grunt-cli`.
2. Navigate to the root directory of your tempate project and run `npm install` which will automatically download and install all of the dependancies that are listed in the package.json file.
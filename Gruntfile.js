'use strict';

module.exports = function (grunt) {

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // load the package.json file as an object that can be referred to
    var pkg = require('./package.json');

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        project: {
            // Configurable paths
            src: 'source',
            dist: 'dist',
            repo: 'TBA'
        },

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            gruntfile: {
                files: ['Gruntfile.js']
            },
            styles: {
                files: ['<%= project.src %>/css/{,*/}*.css'],
                tasks: ['newer:copy:styles', 'autoprefixer']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= project.src %>/{,*/}*.html',
                    '<%= project.src %>/css/{,*/}*.css',
                    '<%= project.src %>/img/{,*/}*.{gif,jpeg,jpg,png,svg,webp}'
                ]
            }
        },

        // Task to inline CSS and send test emails
        premailer: {
            simple: {
                options: {
                    css: ['<%= project.src %>/css/email.css'],
                },
                files : [{
                    expand: true,
                    cwd: '<%= project.src %>/',
                    src: ['*.html'],
                    dest: '<%= project.dist %>/',
                }],
            },
        },


        // Task to deploy the built scripts
        buildcontrol: {
            options: {
                dir: 'dist',
                commit: true,
                push: true,
                message: 'Built %sourceName% from commit %sourceCommit% on branch %sourceBranch%',
            },
            dist: {
                options: {
                    remote: '<%= project.repo %>',
                    branch: 'dist',
                    tag: pkg.version
                }
            }
        },

        // The actual grunt serve settings
        connect: {
            options: {
                port: 9000,
                livereload: 35729,
                // Change this to '0.0.0.0' to access the serve from outside
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    open: 'http://localhost:9000',
                    base: [
                        '.tmp',
                        '<%= project.src %>'
                    ]
                }
            },
            // test: {
            //     options: {
            //         port: 9001,
            //         base: [
            //             '.tmp',
            //             'test',
            //             '<%= project.src %>'
            //         ]
            //     }
            // },
            dist: {
                options: {
                    open: true,
                    base: '<%= project.dist %>',
                    livereload: true
                }
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp/*',
                        '<%= project.dist %>/*',
                        '!<%= project.dist %>/.git*'
                    ]
                }]
            },
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        // jshint: {
        //     options: {
        //         jshintrc: '.jshintrc',
        //         reporter: require('jshint-stylish')
        //     },
        //     all: [
        //         'Gruntfile.js',
        //         // '<%= project.src %>/scripts/{,*/}*.js',
        //     ]
        // },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ['last 2 versions']
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/css/',
                    src: '{,*/}*.css',
                    dest: '.tmp/css/'
                }]
            }
        },


        // The following *-min tasks produce minified files in the dist folder
        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= project.src %>/img',
                    src: '{,*/}*.{gif,jpeg,jpg,png}',
                    dest: '<%= project.dist %>/img'
                }]
            }
        },
        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= project.src %>/img',
                    src: '{,*/}*.svg',
                    dest: '<%= project.dist %>/img'
                }]
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= project.src %>/',
                    dest: '<%= project.dist %>/',
                    src: [
                        '*.{ico,png,txt}',
                        '.htaccess',
                        'images/{,*/}*.webp',
                        'index.html',
                        '../.tmp/css',
                        'css/style.css'
                    ]
                }]
            },
            styles: {
                expand: true,
                dot: true,
                cwd: '<%= project.src %>/css',
                dest: '.tmp/css/',
                src: '{,*/}*.css'
            }
        },


        // Run some tasks in parallel to speed up build process
        concurrent: {
            serve: [
                'copy:styles'
            ],
            dist: [
                'copy:styles',
                'imagemin',
                'svgmin'
            ]
        }
    });


    grunt.registerTask('serve', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'connect:dist:keepalive']);
        }

        grunt.task.run([
            'concurrent:serve',
            'autoprefixer',
            'connect:livereload',
            'watch'
        ]);
    });

    grunt.registerTask('build', [
        'clean:dist',
        'concurrent:dist',
        'autoprefixer',
        'premailer',
        'copy:dist',
    ]);

    grunt.registerTask('deploy', [
        'buildcontrol:dist',
    ]);

    grunt.registerTask('default', [
        'build'
    ]);
};
